#!/bin/bash
Projeto=botWP
VirtualEnvFolder=/home/ec2-user/envs
echo $VirtualEnvFolder
if [ -d "$VirtualEnvFolder/$Projeto" ]; then
	source $VirtualEnvFolder/$Projeto/bin/activate
else
	virtualenv -p /usr/bin/python3 $VirtualEnvFolder/$Projeto
	source $VirtualEnvFolder/$Projeto/bin/activate
	#if [ -f "Requiriments.txt" ]; then
	#	pip install -r Requiriments.txt
	#fi
fi

python main.py


