# -*- coding: utf-8 -*-
#Telegram libs
import threading
import queue
import time
from pprint import pprint

import telepot
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton
import json

#Crie este arquivo com o token obtido pelo seu bot
# e um ID de usuário para retornar as mensagens
from secretes import BOTtoken, userReturn

def read_kbd_input(inputQueue):
	print('Ready for keyboard input:')
	while (True):
		# Receive keyboard input from user.
		input_str = input()

		# Enqueue this input string.
		# Note: Lock not required here since we are only calling a single Queue method, not a sequence of them 
		# which would otherwise need to be treated as one atomic operation.
		inputQueue.put(input_str)




#Whatsapp libs
import re
from bot import wppbot



#whatapp vars

bot = wppbot('ROBerto')
bot.treina('treino')
bot.inicia('ZapBerto')
#bot.saudacao(['Bot: Oi, sou o ROBerto!','Bot: Use ```::``` no início para falar comigo','Bot: ou use ```#``` para enviar mensagens para outros grupos'])
bot.saudacao(['Bot: voltei'])
ultimo_texto = ''


#telegram vars
token = BOTtoken
TelegramBot = telepot.Bot(token)
headerBot = TelegramBot.getMe()
print (headerBot['id'])
update_id = 0

#Keyboard input queue to pass data from the thread reading the keyboard inputs to the main thread.
inputQueue = queue.Queue()

# Create & start a thread to read keyboard inputs.
# Set daemon to True to auto-kill this thread when all other non-daemonic threads are exited. This is desired since
# this thread has no cleanup to do, which would otherwise require a more graceful approach to clean up then exit.
inputThread = threading.Thread(target=read_kbd_input, args=(inputQueue,), daemon=True)
inputThread.start()


while True:

	texto = bot.escuta()
	
	data = TelegramBot.getUpdates(update_id+1)
	if(len(data)):
		update_id = data[0]['update_id']
		pprint(data[0])#['message'])#['message']['text'])
		if 'text' in data[0]['message']:
			texto = data[0]['message']['text'][4:]
			user = data[0]['message']['from']['first_name']
			user += ' '
			user += data[0]['message']['from']['last_name']
			grupo = data[0]['message']['chat']['title']
			msg = '```'
			msg += user
			msg += '``` do grupo ```'
			msg += grupo
			msg += '``` :   '
			msg += texto
			print(msg)
			bot.eco(msg)
		print('\n\n')

	if texto != ultimo_texto and re.match(r'^::', texto):

		ultimo_texto = texto
		texto = texto.replace('::', '')
		texto = texto.lower()

		'''if (texto == 'aprender' or texto == ' aprender' or texto == 'ensinar' or texto == ' ensinar'):
			bot.aprender(texto,'bot: Escreva a pergunta e após o ? a resposta.','bot: Obrigado por ensinar! Agora já sei!','bot: Você escreveu algo errado! Comece novamente..')'''
		'''el'''
		if (texto == 'noticias' or texto == ' noticias' or texto == 'noticia' or texto == ' noticia' or texto == 'notícias' or texto == ' notícias' or texto == 'notícia' or texto == ' notícia'):
			bot.noticias()
		else:
			bot.responde(texto)
	
	elif texto != ultimo_texto and re.match(r'^#', texto):
		ultimo_texto = texto
		texto = texto.replace('#', '')
		texto = texto.lower()
		print(texto)
		TelegramBot.sendMessage(userReturn, texto)

	elif texto != ultimo_texto and re.match(r'^....', texto):
		ultimo_texto = texto
		texto = 'para falar comigo tem que usar :: e não ....'
		bot.eco(texto)

	elif texto != ultimo_texto and re.match(r'^@Guri De Recado', texto):
		ultimo_texto = texto
		texto = texto.replace('@Guri De Recado', '')
		texto = texto.lower()
		print(texto)
		TelegramBot.sendMessage(userReturn, texto)
		#bot.eco(texto)
